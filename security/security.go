/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package security

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"log"
	"math/big"
	"time"
)

var Logger = log.Default()

func init() {
	Logger.SetPrefix("httpserver: ")
}

// get a default host certificate
func GetDefaultHostCert(hostname string) ([]byte, []byte) {
	//https://go.dev/src/crypto/tls/generate_cert.go

	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	// priv, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader) // not supported by chrome
	// pub, priv, err := ed25519.GenerateKey(rand.Reader) // not supported by chrom

	if err != nil {
		Logger.Println("Failed to generate Private Key for default host certificate: ", err)
		return nil, nil
	}

	keyUsage := x509.KeyUsageDigitalSignature
	// keyUsage |= x509.KeyUsageKeyEncipherment // for RSA only

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		Logger.Println("Failed to generate serial number:", err.Error())
		return nil, nil
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			CommonName:   hostname + ".local",
			Organization: []string{"periMICA container"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour * 24),
		KeyUsage:              keyUsage,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}
	template.DNSNames = append(template.DNSNames, hostname)
	template.DNSNames = append(template.DNSNames, hostname+".local")

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	// derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pub, priv)

	if err != nil {
		Logger.Println("Failed to create fallback host certificate: ", err.Error())
		return nil, nil
	}
	certBuffer := &bytes.Buffer{}

	if err := pem.Encode(certBuffer, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		Logger.Println("Failed to pem encode certificate: ", err.Error())
		return nil, nil
	}

	derBytes, err = x509.MarshalECPrivateKey(priv)
	// derBytes, err := x509.MarshalPKCS8PrivateKey(priv) //for ed25519 private key
	if err != nil {
		Logger.Println("Unable to marshal private key: ", err.Error())
		return nil, nil
	}
	keyBuffer := &bytes.Buffer{}
	if err := pem.Encode(keyBuffer, &pem.Block{Type: "EC PRIVATE KEY", Bytes: derBytes}); err != nil {
		Logger.Println("Failed to pem encode private key: ", err.Error())
		return nil, nil
	}

	return certBuffer.Bytes(), keyBuffer.Bytes()
}
