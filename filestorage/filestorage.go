/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

// Utility to store objects persistently in json files.
package filestorage

import (
	"encoding/json"
	"os"
	"path/filepath"
)

var (
	LoadData  = Default_LoadData
	StoreData = Default_StoreData
)

// reads and unmashalls json file from filesystem, relative to BasePath
func LoadObject[T any](path string, obj *T) error {
	var data []byte
	var err error

	data, _ = os.ReadFile(path)

	newObject := *obj

	err = json.Unmarshal(data, &newObject)

	if err == nil {
		*obj = newObject
	}

	return err
}

// serializes object as json and writes to file, relative to BasePath
func StoreObject[T any](path string, obj T) error {
	var data []byte
	var err error

	data, err = json.Marshal(obj)
	if err != nil {
		return err
	}
	// create leading directories if necessary
	dir := filepath.Dir(path)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		os.MkdirAll(dir, 0700) // Create your file
	}
	// file is created, if necessary
	err = os.WriteFile(path, data, 0600)
	return err
}

// reads raw data from filesystem, relative to BasePath
func Default_LoadData(path string, data *[]byte) error {
	var err error

	*data, err = os.ReadFile(path)

	return err
}

// writes data to file, relative to BasePath
func Default_StoreData(path string, data []byte) error {
	var err error

	// create leading directories if necessary
	dir := filepath.Dir(path)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0700) // Create your dir
		if err != nil {
			return err
		}
	}
	// file is created, if necessary
	err = os.WriteFile(path, data, 0600)
	return err
}
