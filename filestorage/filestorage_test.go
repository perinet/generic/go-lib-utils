/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package filestorage

import (
	"os"
	"testing"
	"time"

	"gotest.tools/v3/assert"
)

type mInfo struct {
	Version string `json:"api_version"`

	Config *mConfig `json:"config"`
}

type mConfig struct {
	Name string `json:"app_name"`
}

func TestProductionInfoStorage(t *testing.T) {
	var err error
	now := time.Now()

	config := mConfig{Name: "the name"}
	info := mInfo{Version: "1", Config: &config}
	path := t.TempDir() + "/test.json"

	os.Remove(path)
	var test_info mInfo
	test_info = info
	err = LoadObject(path, &test_info)
	assert.Assert(t, err != nil)
	assert.Assert(t, test_info == info)

	info.Version = now.String()
	err = StoreObject(path, info)
	assert.NilError(t, err)
	test_info = mInfo{Version: "1", Config: &config}
	err = LoadObject(path, &test_info)
	assert.NilError(t, err)
	assert.Assert(t, test_info == info)
}
