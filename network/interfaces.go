/**
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

// network related helper functions
package network

import "net"

type NetworkInfo struct {
	IFace *net.Interface
	IPs   []net.IP
}

// provide network information of local node. It provides a list of interfaces
// including
func GetNetworkInfo() map[string]NetworkInfo {
	infos := make(map[string]NetworkInfo)
	interfaces := listMulticastInterfaces()
	for _, iface := range interfaces {
		ips := []net.IP{}
		ips = append(ips, getLLAddrsFromInterface(&iface)...)
		infos[iface.Name] = NetworkInfo{IFace: &iface, IPs: ips}
	}
	return infos
}

func listMulticastInterfaces() []net.Interface {
	var interfaces []net.Interface
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil
	}
	for _, ifi := range ifaces {
		if (ifi.Flags & net.FlagUp) == 0 {
			continue
		}
		if (ifi.Flags & net.FlagMulticast) > 0 {
			interfaces = append(interfaces, ifi)
		}
	}

	return interfaces
}

func getLLAddrsFromInterface(iface *net.Interface) []net.IP {
	var ipv6 []net.IP
	addrs, _ := iface.Addrs()
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ip := ipnet.IP.To16(); ip != nil && ip.IsLinkLocalUnicast() {
				ipv6 = append(ipv6, ipnet.IP)
			}
		}
	}
	return ipv6
}
