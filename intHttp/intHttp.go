/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package intHttp

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"

	"github.com/gorilla/mux"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
)

func Call(f func(p periHttp.PeriHttp), method string, data io.Reader, vars map[string]string) *http.Response {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest(method, "", data)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	return resp.Result()
}

func CallWithRole(f func(p periHttp.PeriHttp), method string, data io.Reader, vars map[string]string, role rbac.Role) *http.Response {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest(method, "", data)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, role))

	return resp.Result()
}

func Get(f func(p periHttp.PeriHttp), vars map[string]string) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "", nil)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil
		}
		return data
	}

	return nil
}

func Post(f func(p periHttp.PeriHttp), data []byte, vars map[string]string) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "", bytes.NewReader(data))
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK || resp.Code == http.StatusCreated {
		data, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil
		}
		return data
	}

	return nil
}

func Patch(f func(p periHttp.PeriHttp), data []byte, vars map[string]string) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("PATCH", "", bytes.NewReader(data))
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil
		}
		return data
	}

	return nil
}

func Put(f func(p periHttp.PeriHttp), data []byte, vars map[string]string) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "", bytes.NewReader(data))
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil
		}
		return data
	}

	return nil
}

func Delete(f func(p periHttp.PeriHttp), vars map[string]string) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "", nil)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))
}

func Head(f func(p periHttp.PeriHttp), vars map[string]string) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("HEAD", "", nil)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))
}
