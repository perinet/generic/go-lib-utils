/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

// deprecated package
package webhelper

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"

	"github.com/gorilla/mux"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
)

// -------------- response helper --------------

func JsonResponse(w http.ResponseWriter, httpStatusCode int, jsonObject any) {
	var jsonMessage []byte

	switch object := jsonObject.(type) {
	case []byte:
		jsonMessage = object
	default:
		var err error
		jsonMessage, err = json.Marshal(object)
		if err != nil {
			w.Header().Set("Content-type", "application/json; charset=utf-8;")
			w.WriteHeader(http.StatusInternalServerError)
			w.Write(json.RawMessage(`{"error": "cannot marshal object to json"}`))
			return
		}

	}

	w.Header().Set("Content-type", "application/json; charset=utf-8;")
	w.WriteHeader(httpStatusCode)
	w.Write(jsonMessage)
}

func TextResponse[T []byte | string](w http.ResponseWriter, httpStatusCode int, textMessage T) {
	w.Header().Set("Content-type", "text/plain; charset=utf-8;")
	w.WriteHeader(httpStatusCode)
	w.Write([]byte(textMessage))
}

func BinaryResponse(w http.ResponseWriter, httpStatusCode int, data []byte) {
	w.Header().Set("Content-type", "application/octet-stream")
	w.WriteHeader(httpStatusCode)
	w.Write(data)
}

func EmptyResponse(w http.ResponseWriter, httpStatusCode int) {
	w.WriteHeader(httpStatusCode)
}

// -------------- internal call helper --------------

func InternalGeneric(f func(p periHttp.PeriHttp), method string, data io.Reader) *http.Response {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest(method, "", data)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	return resp.Result()
}

func InternalVarsGeneric(f func(p periHttp.PeriHttp), method string, data io.Reader, vars map[string]string) *http.Response {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest(method, "", data)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	return resp.Result()
}

func InternalGet(f func(p periHttp.PeriHttp)) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "", nil)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, _ := io.ReadAll(resp.Body)
		return data
	}

	return nil
}

func InternalVarsGet(f func(p periHttp.PeriHttp), vars map[string]string) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "", nil)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, _ := io.ReadAll(resp.Body)
		return data
	}

	return nil
}

func InternalPost(f func(p periHttp.PeriHttp), data []byte) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "", bytes.NewReader(data))
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK || resp.Code == http.StatusCreated {
		data, _ := io.ReadAll(resp.Body)
		return data
	}

	return nil
}

func InternalVarsPost(f func(p periHttp.PeriHttp), vars map[string]string, data []byte) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "", bytes.NewReader(data))
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK || resp.Code == http.StatusCreated {
		data, _ := io.ReadAll(resp.Body)
		return data
	}

	return nil
}

func InternalPatch(f func(p periHttp.PeriHttp), data []byte) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("PATCH", "", bytes.NewReader(data))
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, _ := io.ReadAll(resp.Body)
		return data
	}

	return nil
}

func InternalVarsPatch(f func(p periHttp.PeriHttp), vars map[string]string, data []byte) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("PATCH", "", bytes.NewReader(data))
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, _ := io.ReadAll(resp.Body)
		return data
	}

	return nil
}

func InternalPut(f func(p periHttp.PeriHttp), data []byte) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "", bytes.NewReader(data))
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, _ := io.ReadAll(resp.Body)
		return data
	}

	return nil
}

func InternalVarsPut(f func(p periHttp.PeriHttp), vars map[string]string, data []byte) []byte {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "", bytes.NewReader(data))
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))

	if resp.Code == http.StatusOK {
		data, _ := io.ReadAll(resp.Body)
		return data
	}

	return nil
}

func InternalDelete(f func(p periHttp.PeriHttp)) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "", nil)
	f(periHttp.New(resp, req, rbac.INTERNAL))
}

func InternalVarsDelete(f func(p periHttp.PeriHttp), vars map[string]string) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "", nil)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))
}

func InternalHead(f func(p periHttp.PeriHttp)) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("HEAD", "", nil)
	f(periHttp.New(resp, req, rbac.INTERNAL))
}

func InternalVarsHead(f func(p periHttp.PeriHttp), vars map[string]string) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("HEAD", "", nil)
	req = mux.SetURLVars(req, vars)
	f(periHttp.New(resp, req, rbac.INTERNAL))
}
