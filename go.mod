module gitlab.com/perinet/generic/lib/utils

go 1.18

require (
	github.com/gorilla/mux v1.8.1
	gotest.tools/v3 v3.5.1
)

require github.com/google/go-cmp v0.5.9 // indirect
