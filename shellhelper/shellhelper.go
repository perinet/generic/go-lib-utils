/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package shellhelper

import (
	"bytes"
	"io"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
)

type CmdResult struct {
	Stdout   string
	Stderr   string
	Exitcode int
}

type CmdBinaryResult struct {
	Stdout   []byte
	Stderr   []byte
	Exitcode int
}

var (
	WriteFile                  = Default_WriteFile
	FileExist                  = Default_FileExist
	ShellCall                  = Default_ShellCall
	ShellCallAsUser            = Default_ShellCallAsUser
	ShellCallWithInput         = Default_ShellCallWithInput
	AsyncShellCall             = Default_AsyncShellCall
	AsyncShellCallAsUser       = Default_AsyncShellCallAsUser
	CommandCall                = Default_CommandCall
	CommandCallAsUser          = Default_CommandCallAsUser
	CommandCallWithInput       = Default_CommandCallWithInput
	BinaryCommandCall          = Default_BinaryCommandCall
	BinaryCommandCallAsUser    = Default_BinaryCommandCallAsUser
	BinaryCommandCallWithInput = Default_BinaryCommandCallWithInput
)

func Default_WriteFile(file_name string, c string, args ...fs.FileMode) error {
	dir := filepath.Dir(file_name)
	mode := os.ModePerm
	if len(args) > 0 {
		mode = args[0]
	}
	os.MkdirAll(dir, os.ModePerm)
	f, err := os.OpenFile(file_name, os.O_RDWR|os.O_CREATE|os.O_TRUNC, mode)

	if err != nil {
		log.Println(err)
		return err
	}

	defer f.Close()

	_, err2 := f.WriteString(c)

	if err2 != nil {
		log.Println(err2)
		return err2
	}

	return nil
}

func Default_FileExist(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func Default_ShellCall(cmd_ string) string {
	c := exec.Command("sh", "-c", cmd_)
	stdout, err := c.CombinedOutput()
	if err != nil {
		log.Println(err, ": ", string(stdout))
		return ""
	}

	return string(stdout)
}

func Default_ShellCallAsUser(username string, cmd_ string) string {
	uid, gid, err := getUserId(username)
	if err != nil {
		log.Println(err)
		return ""
	}

	cmd := exec.Command("sh", "-c", cmd_)
	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(uid), Gid: uint32(gid)}

	stdout, err := cmd.CombinedOutput()
	if err != nil {
		log.Println(err, ": ", string(stdout))
		return ""
	}

	return string(stdout)
}

func Default_ShellCallWithInput(input io.Reader, cmd_ string) string {
	c := exec.Command("sh", "-c", cmd_)
	c.Stdin = input
	stdout, err := c.CombinedOutput()
	if err != nil {
		log.Println(err, ": ", string(stdout))
		return ""
	}

	return string(stdout)
}

func Default_AsyncShellCall(cmd_ string) *exec.Cmd {
	cmd := exec.Command("sh", "-c", cmd_)
	err := cmd.Start()
	if err != nil {
		log.Println(err)
		return nil
	}
	return cmd
}

func Default_AsyncShellCallAsUser(username string, cmd_ string) *exec.Cmd {
	uid, gid, err := getUserId(username)
	if err != nil {
		log.Println(err)
		return nil
	}

	cmd := exec.Command("sh", "-c", cmd_)
	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(uid), Gid: uint32(gid)}

	err = cmd.Start()
	if err != nil {
		log.Println(err)
		return nil
	}
	return cmd
}

func Default_CommandCall(name string, arg ...string) CmdResult {
	cmd := exec.Command(name, arg...)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	exitCode := 0
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			exitCode = exitError.ExitCode()
		} else {
			log.Println(err)
			return CmdResult{}
		}
	}

	return CmdResult{Stdout: strings.TrimSuffix(stdout.String(), "\n"), Stderr: strings.TrimSuffix(stderr.String(), "\n"), Exitcode: exitCode}
}

func Default_CommandCallAsUser(username string, name string, arg ...string) CmdResult {
	uid, gid, err := getUserId(username)
	if err != nil {
		log.Println(err)
		return CmdResult{}
	}

	cmd := exec.Command(name, arg...)
	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(uid), Gid: uint32(gid)}

	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()

	exitCode := 0
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			exitCode = exitError.ExitCode()
		} else {
			log.Println(err)
			return CmdResult{}
		}
	}

	return CmdResult{Stdout: strings.TrimSuffix(stdout.String(), "\n"), Stderr: strings.TrimSuffix(stderr.String(), "\n"), Exitcode: exitCode}
}

func Default_CommandCallWithInput(input io.Reader, name string, arg ...string) CmdResult {
	cmd := exec.Command(name, arg...)
	var stdout, stderr bytes.Buffer
	cmd.Stdin = input
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	exitCode := 0
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			exitCode = exitError.ExitCode()
		} else {
			log.Println(err)
			return CmdResult{}
		}
	}

	return CmdResult{Stdout: strings.TrimSuffix(stdout.String(), "\n"), Stderr: strings.TrimSuffix(stderr.String(), "\n"), Exitcode: exitCode}
}

func Default_BinaryCommandCall(name string, arg ...string) CmdBinaryResult {
	cmd := exec.Command(name, arg...)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	exitCode := 0
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			exitCode = exitError.ExitCode()
		} else {
			log.Println(err)
			return CmdBinaryResult{}
		}
	}
	return CmdBinaryResult{Stdout: stdout.Bytes(), Stderr: stderr.Bytes(), Exitcode: exitCode}
}

func Default_BinaryCommandCallAsUser(username string, name string, arg ...string) CmdBinaryResult {
	uid, gid, err := getUserId(username)
	if err != nil {
		log.Println(err)
		return CmdBinaryResult{}
	}

	cmd := exec.Command(name, arg...)
	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(uid), Gid: uint32(gid)}

	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()

	exitCode := 0
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			exitCode = exitError.ExitCode()
		} else {
			log.Println(err)
			return CmdBinaryResult{}
		}
	}
	return CmdBinaryResult{Stdout: stdout.Bytes(), Stderr: stderr.Bytes(), Exitcode: exitCode}
}

func Default_BinaryCommandCallWithInput(input io.Reader, name string, arg ...string) CmdBinaryResult {
	cmd := exec.Command(name, arg...)
	var stdout, stderr bytes.Buffer
	cmd.Stdin = input
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	exitCode := 0
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			exitCode = exitError.ExitCode()
		} else {
			log.Println(err)
			return CmdBinaryResult{}
		}
	}
	return CmdBinaryResult{Stdout: stdout.Bytes(), Stderr: stderr.Bytes(), Exitcode: exitCode}
}

func getUserId(username string) (uint32, uint32, error) {
	u, err := user.Lookup(username)
	if err != nil {
		log.Println(err)
		return 0, 0, err
	}

	uid, _ := strconv.ParseInt(u.Uid, 10, 32)
	gid, _ := strconv.ParseInt(u.Gid, 10, 32)

	return uint32(uid), uint32(gid), nil
}
